# frozen_string_literal: true

# This class is a helper for testing the CLI. It captures the stdout
# stream and returns the output printed by the CLI.
# This class is not thread safe
class StdOutCapturer
  def self.capture(&block)
    new.capture(&block)
  end

  def capture
    original = $stdout
    out = StringIO.new
    $stdout = out
    res = yield
    $stdout = original
    [res, out.string]
  end
end
