# frozen_string_literal: true

require 'net/http'

# Provides helpers to mock HTTP reponses
module HTTPFakes
  FakeHTTPResponse = Struct.new(:code, :body)
  def fake_response(filename, status_code = '200')
    return FakeHTTPResponse.new(status_code, nil) if filename.nil?

    path = File.join(File.dirname(__FILE__), '..', 'fixtures', filename)
    FakeHTTPResponse.new(status_code, File.read(path))
  end
end
