# frozen_string_literal: true

require 'helper/http_fakes'
require 'eltiempo'

RSpec.describe Boris::ElTiempoAPI do
  let(:http_client) { instance_double(Boris::HTTPClient) }

  let(:city_name) { 'Gavà' }

  let(:api) { described_class.new(102, 'abc', http_client) }

  before do
    allow(http_client).to receive(:get).and_return(fake_response('cities.xml'))
  end

  context 'with an invalid city name' do
    let(:city_name) { 'Madrid' }

    it 'throws an exception' do
      expect { api.forecast_for(city_name) }.to raise_error(Boris::InvalidCityError)
    end
  end

  context 'with an error from the cities API' do
    before do
      allow(http_client).to receive(:get).and_return(fake_response(nil, 500))
    end

    it 'throws an exception' do
      expect { api.forecast_for(city_name) }.to raise_error(Boris::ProviderError)
    end
  end

  context 'with an error from the forecast API' do
    before do
      allow(http_client).to receive(:get).and_return(
        fake_response('cities.xml'), fake_response(nil, 500)
      )
    end

    it 'throws an exception' do
      expect { api.forecast_for(city_name) }.to raise_error(Boris::ProviderError)
    end
  end
end
