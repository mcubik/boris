# frozen_string_literal: true

require 'cli'
require 'eltiempo'

RSpec.describe Boris::Cli do
  let(:provider) { instance_double(Boris::ElTiempoAPI) }
  let(:cli) { described_class.new(provider) }

  before do
    allow(provider).to receive(:forecast_for)
  end

  it 'returns an error if the command is not recognized' do
    status, output = StdOutCapturer.capture { cli.run(['-bad', 'Some city']) }

    expect([status, output]).to eq [
      3, "Invalid command. Use boris [-today | -av_min | -av_max] city_name\n"
    ]
  end

  it 'returns an error if the arguments are missing' do
    status, output = StdOutCapturer.capture { cli.run(['-today']) }

    expect([status, output]).to eq [
      3, "Invalid command. Use boris [-today | -av_min | -av_max] city_name\n"
    ]
  end
end
