# frozen_string_literal: true

RSpec.describe Boris::ForecastParser do
  let(:filename) { 'forecast.xml' }

  let(:xml) do
    # FIXME: Improve this file loading code
    File.join(File.dirname(__FILE__), '../..', 'fixtures', filename)
        .then { |path| File.read(path) }
  end

  it 'parses the forecast xml' do
    expect(described_class.parse(xml)).to have_attributes(
      mins: [3, -1, 4, 4, 4, 5, 6],
      maxs: [15, 17, 20, 15, 20, 21, 19]
    )
  end
end
