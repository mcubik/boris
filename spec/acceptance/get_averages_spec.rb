# frozen_string_literal: true

require 'helper/stdout_capturer'
require 'helper/http_fakes'
require 'main'

# We just want to know the average of the minimum and maximum temperature during the week,
# and the temperature of the day.
# We could run it like this:
# eltiempo -today 'Gava'
RSpec.describe 'Get todays temperature' do
  RSpec.configure do |c|
    c.include HTTPFakes
  end

  let(:cities_uri) do
    'http://api.tiempo.com/index.php?api_lang=es'\
    '&division=102&affiliate_id=zdo2c683olan'
  end

  let(:forecast_uri) do
    'http://api.tiempo.com/index.php?api_lang=es'\
    '&localidad=1381&affiliate_id=zdo2c683olan'
  end

  let(:http_client) { instance_double(Boris::HTTPClient) }

  let(:cli) do
    client = http_client # RSpec magic doesn't work inside nested blocks
    Boris::ApplicationBuilder.build_cli do
      with_http_client client
      with_eltiempo_provider 102, 'zdo2c683olan'
    end
  end

  let(:city_name) { 'Gavà' }

  # This mocking wouldn't be necessariy if the API provided
  # a Sandbox or some form of repdocible result.
  before do
    allow(http_client).to receive(:get).with(cities_uri).and_return(
      fake_response('cities.xml')
    )

    allow(http_client).to receive(:get).with(forecast_uri).and_return(
      fake_response('forecast.xml')
    )
  end

  it 'calculates the average minumum' do
    status, output = StdOutCapturer.capture do
      cli.run(['-av_min', city_name])
    end

    expect([status, output]).to eq [
      0, "The average minimum for the next week is 3.57\n"
    ]
  end

  it 'calculates the average maximum' do
    status, output = StdOutCapturer.capture do
      cli.run(['-av_max', city_name])
    end

    expect([status, output]).to eq [
      0, "The average maximum for the next week is 18.14\n"
    ]
  end
end
