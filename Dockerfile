FROM ruby:3.0.0

ADD . /app

WORKDIR /app

RUN gem build boris.gemspec
RUN gem install boris-0.0.0.gem
