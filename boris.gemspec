# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'boris'
  s.version     = '0.0.0'
  s.required_ruby_version = '3.0.0'
  s.summary     = 'Paack test'
  s.description = 'Paack test'
  s.authors     = ['Matías Castilla']
  s.email       = 'matiascastilla@gmail.com'
  s.files       = Dir.glob('{bin|lib}/**/*')
  s.executables = ['boris']
  s.add_dependency 'nokogiri', '1.11.2'
end
