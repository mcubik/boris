# frozen_string_literal: true

require 'net/http'

module Boris
  # HTTPClient is a basic wrapper around net/http. It's used to ease
  # testing as net/http API is static in nature.
  class HTTPClient
    STATUS_OK = 200

    # Wrapper around Net::HTTP.get_reponse
    # @param uri [String]
    # @return [Net::HTTP::reponse]
    def get(uri)
      Net::HTTP.get_response(URI(uri))
    end
  end
end
