# frozen_string_literal: true

require 'http_client'
require 'eltiempo'
require 'cli'

# Main module is a concept of Clean Architecture.
# The idea is to have a module that builds the application by
# doing the depdenency injection and tying all the components.
# I like to do that with a builder and avoid a DIC if possible
# (dependency injection containers are uncommon
# in Ruby, anyway)
# TODO: Unit test this class.
# For the sake of time, I'm not testing it. The test would consist
# basically of veryfing that the Cli object is correctly build
# and that the error cases like not passing a block or not initializing
# some of the components are correctly handled.
# The best way to add the error cases is test-driven
#
module Boris
  # Builds the application
  class ApplicationBuilder
    # Builds the application CLI using a block
    # @param  instructions [Block] build instructions
    # @return Cli
    def self.build_cli(&instructions)
      new.build_cli(&instructions)
    end

    # Initialize components
    def initialize
      @forecast_provider = nil
      @http_client = nil
    end

    # Builds the Cli using the instructions in +instructions+
    # @param instructions[Block] a block that calls the buid methods
    # @return Cli
    def build_cli(&instructions)
      raise StandardError, 'Missing build instructions' unless block_given?

      instance_eval(&instructions)
      raise StandardError, 'Forecast provider not specified' if @forecast_provider.nil?

      Cli.new(@forecast_provider)
    end

    private

    def with_eltiempo_provider(area_code, affiliate_id)
      @forecast_provider = ElTiempoAPI.new(area_code, affiliate_id, @http_client)
    end

    def with_http_client(http_client)
      @http_client = http_client
    end
  end
end
