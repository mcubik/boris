# frozen_string_literal: true

require 'nokogiri'
require 'http_client'
require 'provider'

module Boris
  # ElTimpo provides forecasts throught the El Tiempo API
  # It is an infrastructure component which in statically typed language
  # would implement a ForecastProvider interface defined in the domain
  class ElTiempoAPI
    BASE_URL = 'http://api.tiempo.com/index.php?api_lang=es'

    # The dependencies can be injected in the constructor
    # @param area_code [String]. The code of the area we are providing forecasts for
    # @param affiliate_id [String]. Is the token to access the ElTiempo API
    # @param http_client [HTTPClient]. The HTTPClient used to call the API
    def initialize(area_code, affiliate_id, http_client = nil)
      @area_code = area_code
      @affiliate_id = affiliate_id
      @http_client = http_client || HTTPClient.new
    end

    # Returns the forecast for the with the given +city_name+
    # @param city_name [String] the name of the city
    # @return [Array] the max and min temperatures
    # @throws [InvalidCityError] if the city doesn't exist
    # @throws [ProviderError] if the API returns an error
    def forecast_for(city_name)
      get_city_code(city_name).then { |code| get_forecast(code) }
    end

    private

    # TODO: I don't rembeber exactly how the net/http API works
    # but if it raises exceptions then I should cactch them and
    # convert into ProviderErrors
    def get_city_code(city_name)
      response = @http_client.get(cities_uri)

      # FIXME: This check is not idiomatic in Ruby
      if response.code.to_i != HTTPClient::STATUS_OK
        # TODO: Here all the error information should be logged
        # I'm not going to handle logging for the sake of time
        raise ProviderError, 'Forecast provider failed'
      end

      cities_doc = Nokogiri::XML(response.body)

      data_node = cities_doc
                  .at_xpath("/report/location/data/name[text() = '#{city_name}']")

      raise InvalidCityError, city_name if data_node.nil?

      data_node['id']
    end

    def get_forecast(city_code)
      response = @http_client.get(forecast_uri(city_code))

      if response.code.to_i != HTTPClient::STATUS_OK
        # TODO: Here all the error information should be logged
        # I'm not going to handle logging for the sake of time
        raise ProviderError, 'Forecast provider failed'
      end

      ForecastParser.parse(response.body)
    end

    # TODO: Here I should use the net/http api URI
    # object to construct the query instead of directly
    # concatenete the string.
    def cities_uri
      "#{BASE_URL}"\
          "&division=#{@area_code}"\
          "&affiliate_id=#{@affiliate_id}"
    end

    def forecast_uri(city_code)
      "#{BASE_URL}"\
        "&localidad=#{city_code}"\
        "&affiliate_id=#{@affiliate_id}"
    end
  end

  Forecast = Struct.new(:mins, :maxs)

  # Parses the forecast XML
  class ForecastParser
    # Static/class entrypoint for parsing the forecast XML
    def self.parse(xml)
      new(xml).parse
    end

    private_class_method :new

    # Initalizes the parser with the XML string
    # @param xml the XML string
    def initialize(xml)
      @document = Nokogiri::XML(xml)
    end

    # Parses the XML and returns a forecast
    # @return [Forecast] the parsed forecast
    def parse
      mins = @document.at_xpath(
        "//report/location/var[name[text() = 'Temperatura Mínima']]/data"
      ).children.map { |e| e['value'].to_i }

      maxs = @document.at_xpath(
        "//report/location/var[name[text() = 'Temperatura Máxima']]/data"
      ).children.map { |e| e['value'].to_i }

      Forecast.new(mins, maxs)
    end
  end
end

# Extends array with an average method
class Array
  # Returns the average of the values in the array rounded to 2 decimals
  # @return [Float] with 2 decimals
  def avg
    (sum / size.to_f).round(2)
  end
end
