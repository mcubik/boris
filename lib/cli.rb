# frozen_string_literal: true

require 'provider'

module Boris
  # Cli
  # Command Line Interface for the forecaster
  # At this 'walking skeleton 'stage it corries out all the responsibilities
  # of the application.
  # rubocop:disable all
  class Cli
    EXIT_STATUS_SUCCESS = 0
    EXIT_STATUS_INVALID_CITY = 1
    EXIT_STATUS_ERROR = 2
    EXIT_STATUS_INVALID_COMMAND = 3

    CMD_TODAY_TEMP = '-today'
    CMD_AVG_MIN = '-av_min'
    CMD_AVG_MAX = '-av_max'

    # The dependencies can be injected in the constructor
    # @param forecast_provider. The forecast provider
    def initialize(forecast_provider)
      @forecast_provider = forecast_provider
    end

    # Executes the CLI and returns a status code
    # Directly prints to stdout (possible alternatives: inject to stdout stream or
    # return the text to print)
    # @args [Array] the command line arguments, as received by the Ruby interpreter
    # @return [Integer] a status code to be returned by the program
    def run(args)
      if args.size != 2
        usage
        return EXIT_STATUS_INVALID_COMMAND
      end

      command = args[0]

      city_name = args[1]

      begin
        forecast = @forecast_provider.forecast_for(city_name)
        case command
        when CMD_TODAY_TEMP
          puts("Today's temperature for #{city_name} is: "\
               "minimum #{forecast.mins.first}, maximum #{forecast.maxs.first}")
        when CMD_AVG_MIN
          puts("The average minimum for the next week is #{forecast.mins.avg}")
        when CMD_AVG_MAX
          puts("The average maximum for the next week is #{forecast.maxs.avg}")
        else
          usage
          return EXIT_STATUS_INVALID_COMMAND
        end

        EXIT_STATUS_SUCCESS

      rescue InvalidCityError
        puts("I couldn't find a forecast for '#{city_name}'")
        EXIT_STATUS_INVALID_CITY
      rescue => e
        puts("An error ocurred: #{e.message}. See the log for details")
        EXIT_STATUS_ERROR
      end
    end

    private

    def usage
      puts('Invalid command. Use boris [-today | -av_min | -av_max] city_name')
    end
  end
end
