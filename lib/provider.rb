# frozen_string_literal: true

# This module belongs to the domain and defines the protocol of
# a forecast provider. Concretely, how to notify errors.
module Boris
  # This exception is raised when the provider doesn't find
  # a forecast for the city.
  InvalidCityError = Class.new(StandardError)

  # This exception is raised when the provider API
  # fails.
  ProviderError = Class.new(StandardError)
end
