# Tech Test - Matías Castilla

> Boris has just given me a summary of his views. He is a weather prophet. The weather will continue bad, he says. [...] The weather will not change.

– Henry Miller, *Tropic of Cancer*

## Requirements

> We would like to code a CLI in ruby to know the weather of the cities of Barcelona using the
> 'eltiempo.com' api._i
> This is the api url for the cities of Barcelona:
> http://api.tiempo.com/index.php?api_lang=es&division=102
> To get access to the api you can use our identification, with the param "affiliate_id".
> Our id is ​ zdo2c683olan
> We just want to know the average of the minimum and maximum temperature during the week,
> and the temperature of the day.
> We could run it like this:
> `eltiempo -today 'Gavá'`
> `eltiempo -av_max 'Gavá'`
> `eltiempo -av_min 'Gavá'`

### Summary

Given a city:

- Return tempreature for today
- Return the average maximum for the next 7 days
- return the average minimum for the next 7 days

### Assumptions

- I couldn't find an endpoint to retrieve a signle value for the temperature of the day. Therefore
I assumed that I had to return the maximum and minimum of the day
- By *during the week* I interpreted *the next 7 days*, as that is the data the the API returns

## Development Approach

I used TDD -London School style- to develop the application. You can follow the development process through the commits (I used a commit summary and a long comment to clearly explain each step).

I created a commit for each TDD step and a branch and a merge requests for each feature. I can provide
access to the Gitlab repository if you want.

I used some basic quality harness that consisted on three checks implemented as Rake tasks:
- Automated tests
- Linter (Rubocop, I excluded some checks for tests. Please take a look at `.rubocop.yml`)
- Coverage (coverage ended up being 99.56%)
- CI in Gitlab that runs all these checks.

There's a lot of place for improvement in the pipeline.

### Testing strategy

I usually use a pyramid approeach for testing. Provided the simplicity of the task, I ended up
with just two levels of tests:

- Acceptance tests for the use cases/features
- Unit tests for testing the individual classes

It wasn't possible to integration-test the API as it doesn't provide a sandbox enviroment or
reproducible endpoints (more about this in the code documentation).

### Archictecture

I tried to use an architecture based on concepts of DDD/Clean Architecture/Hexagonal Architecture.
This is the apporeach I developed over the years for my projects. Of course, it's an overkill
for the very basic required functionality, but I tried to showcase what I regard as good engineering
practices with a highly decoupled design.
In the real world I can be happy with any architecture ranging from a "transaction script" to a fully
decoupled object model if it's sufficiently justified by a tradeoff analysis.

I could have used a `domain`, `infrastructure`, `application` directory structure, but for small projects I
prefer to just document the type of the components.

The application has four components:
- The CLI, infrastructure component to access the application.
- The forecast provider (`ElTiempoAPI`) A component defined at the domain level (an interface in a static type language) implemented
in infrastructure.
- An http client: An infrastructure component I created to ease testing due to limitations of the `net/http` API
(and my personal aversion to mock static/class level stuff).
- A main component or application builder

I didn't use any application component (like use cases interactors or application service as that would have looked too bloated).

I would have expected to end up with some kind of `Forecaster` component as a main domain component providing
the business logic for the main use cases. However when I finished the last use case it just looked fine. It would
be very easy to refactor into such a component the very thin business logic that is splitted between the `Cli` and
the forecast provider.

## Pending

I had planned to do some improvements and optimizations if I had more time:

- Ignore case and special characters (like accents) when looking for the city: This can be worked directly in the provider with unit tests.
- Cache the cities reponses: the cities are unlikely to change, and therefore a simple optimization could have been to cache them.
This can be easily achieved with a wrapper around `HTTPClient`. The cached values can be saved into a file in a folder `.boris` in the home
directory.
- Resiliency patterns: At least use a timeout in the HTTP client to avoid hanging the application.
- Configuration of the token and the area code over a file (I think this appreoach is bettern than environment variables for a cli).
- There are a bunch  TODOs and FIXMEs scattered in the code, detailing other pending work.
- Remove duplication in acceptance tests (everything related with the mocked `HTTPClient` can be factored out).
- Try new Ruby 3 type specification :)

## Instalation

The command is provided by a Gem. Therefore, the Gem has to be installed from the source. I provided
a Docker image that installs the Gem.

To build it run:

`docker build -t boris .`

Now you can execute the command:

```bash
docker run --rm boris sh -c "boris -today 'Gavà'"
docker run --rm boris sh -c "boris -av_min 'Gavà'"
docker run --rm boris sh -c "boris -av_max 'Gavà'"
```


# Notice

The first two commits are signed with a different name as this is the username in my freshly installed PC and I forgot to
configure git settings 🤦
